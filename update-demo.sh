#!/bin/bash
# this script is used on cipherbliss.com to show a demo, customize it to make your own demo
sh up_change.sh > CHANGELOG.md

git pull origin develop
yarn install --pure-lockfile
yarn build:demobliss
sudo cp -r ./dist/framadate/* ../framadate-api/public/
echo " now check update demo at https://framadate-api.cipherbliss.com"
