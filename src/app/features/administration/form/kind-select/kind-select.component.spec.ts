import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KindSelectComponent } from './kind-select.component';

describe('KindSelectComponent', () => {
	let component: KindSelectComponent;
	let fixture: ComponentFixture<KindSelectComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [KindSelectComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KindSelectComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
